package com.rave.greetings

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class GreetingTest {

    private val greeting = Greeting()

    @Test
    @DisplayName("Test when we have a name")
    fun requirement1() {
        // Given
        val name = "Dan"

        // When
        val message = greeting.greet(name)

        // Then
        Assertions.assertEquals("Hello, $name.", message)
    }

    @Test
    @DisplayName("Test when name is null")
    fun requirement2() {
        // Given
        val name : String? = null

        // When
        val message = greeting.greet(name)

        // Then
        Assertions.assertEquals("Hello, my friend.", message)
    }

    @Test
    @DisplayName("Test when name is all uppercase")
    fun requirement3() {
        // Given
        val name = "JERRY"

        // When
        val message = greeting.greet(name)

        // Then
        Assertions.assertEquals("HELLO $name!", message)
    }

    @Test
    @DisplayName("Test handling two names as input")
    fun requirement4() {
        // Given
        val name = arrayOf("Jill", "Jane")

        // When
        val message = greeting.greet(name)

        // Then
        Assertions.assertEquals("Hello, ${name[0]} and ${name[1]}.", message)
    }

    @Test
    @DisplayName("Test arbitrary number of names as input")
    fun requirement5() {
        // Given
        val name = arrayOf("Amy", "Brian", "Charlotte")

        // When
        val message = greeting.greet(name)

        // Then
        Assertions.assertEquals("Hello, Amy, Brian, and Charlotte.", message)
    }

    @Test
    @DisplayName("Test mix of normal and shouted names")
    fun requirement6() {
        // Given
        val name = arrayOf("Amy", "BRIAN", "Charlotte")

        // When
        val message = greeting.greet(name)

        // Then
        Assertions.assertEquals("Hello, Amy and Charlotte. AND HELLO BRIAN!", message)
    }

    @Test
    @DisplayName("Test entries containing comma")
    fun requirement7() {
        // Given
        val name = arrayOf("Bob", "Charlie, Dianne")

        // When
        val message = greeting.greet(name)

        // Then
        Assertions.assertEquals("Hello, Bob, Charlie, and Dianne.", message)
    }

    @Test
    @DisplayName("Test escaping intentional commas")
    fun requirement8() {
        // Given
        val name = arrayOf("Bob", "\"Charlie, Dianne\"")

        // When
        val message = greeting.greet(name)

        // Then
        Assertions.assertEquals("Hello, Bob and Charlie, Dianne.", message)
    }
}