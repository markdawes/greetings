package com.rave.greetings

// Type Inferance: Kotlin compiler can automatically identify the data type of the variable
var x = 5 // Kotlin knows this is an Int

// Null safety: Kotlin requires values that may be null to be declared as nullable to prevent null pointer exceptions
var name: String? = null

// Sealed class: Sealed classes ensure type safety by restricting the set of types at compile-time only.
sealed class A{
    class B : A()
    {
        class E : A()
    }
    class C : A()

    init {
        println("sealed class A")
    }
}

// Data class: Data classes allow us to hold data without the boilerplate code needed in Jave. No need for setters or getters
data class Task(
    var id: Int,
    var description: String,
    var priority: Int
)

// Lazy: To create an object that will be initialized at the first access to it, we can use the lazy method
data class User(val id : Long,
                val username : String)

val user : User by lazy {
    //can do other initialisation here
    User(id = 1001, username = "ballu")
}

// Late init: the initial value does not need to be assigned
lateinit var myVariable: String

// Scope Functions: used to execute a block of code within the scope of an object
fun main() {
    // nullable variable a
    // with value as null
    var a: Int? = null
    // using let function
    a?.let {
        // statement(s) will
        // not execute as a is null
        print(it)
    }
    // re-initializing value of a to 2
    a = 2
    a?.let {
        // statement(s) will execute
        // as a is not null
        print(a)
    }
}

// Extension Functions: A way of extending existing classes with new functionality without using inheritance
// example: apply
fun example() {
    val languages = mutableListOf<String>()
    languages.apply{
        add("Java")
        add("Kotlin")
        add("Groovy")
        add("Python")
    }.apply{
        remove("Python")
    }
}

