package com.rave.greetings

class Greeting {
    fun greet(name: String?): String {
        val updatedName = if (name == null) "my friend" else name
        if (updatedName == updatedName.uppercase()) {
            return "HELLO $updatedName!"
        } else {
            return "Hello, $updatedName."
        }
    }

    fun greet(names: Array<String>): String {
        if (names.size == 2){
            val joinedNames = names.joinToString(" and ")
            return greet(joinedNames)
        } else if (names.size > 2) {
            var message = "Hello, "
            var count = 0
            while (count < names.size-1){
                message+="${names[count]}, "
                count++
            }
            message+="and ${names[names.size-1]}."
            return message
        }
        return ""
    }

}